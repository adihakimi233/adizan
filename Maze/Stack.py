"""
Authors: Adi Hakimi - 209445121, Nizan Shookroun - 207678699
Date: 10/09/2022
Description: Finds a solution path in a given maze
"""


class Node:
    """
    Class to create nodes of linked list
    constructor initializes node automatically
    """
    def __init__(self, data):
        self.data = data
        self.next = None


class Stack:
    def __init__(self):
        self.head = None

    def is_empty(self):
        """
        Method to check if stack is empty
        :return: true if empty, else false
        """

        if self.head is None:
            return True
        else:
            return False

    def push(self, data):
        """
        Method to add data to the start of the stack
        :param data: value to add
        :return: Nothing
        """

        if self.head is None:
            self.head = Node(data)

        else:
            new_node = Node(data)
            new_node.next = self.head
            self.head = new_node

    def pop(self):
        """
        Methos to remove the head element from stack
        :return: element of stack or None if empty
        """

        if self.is_empty():
            print("Stack Underflow")
        else:
            # Removes the head node and makes
            # the preceding one the new head
            popped_node = self.head
            self.head = self.head.next
            popped_node.next = None
            return popped_node.data

    def peek(self):
        """
        Method to print the head of the stack
        :return: head of stack
        """
        if self.is_empty():
            return None
        else:
            return self.head.data

    def display(self):
        """
        Methos that prints the stack
        :return: print of stack
        """
        iter_node = self.head
        if self.is_empty():
            print("Stack Underflow")
        else:
            while iter_node is not None:
                print(iter_node.data, "<-", end=" ")
                iter_node = iter_node.next
            return
