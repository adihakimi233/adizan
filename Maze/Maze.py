"""
Authors: Adi Hakimi - 209445121, Nizan Shookroun - 207678699
Date: 10/09/2022
Description: Finds a solution path in a given maze
"""

from Stack import Stack


def is_safe(Maze, x, y):
    """
    This function checks if the point is in array limits and its value is 0
    :param Maze: 2 dimensional array
    :param x: x point
    :param y: y point
    :return: true if in limits and 0, else false
    """
    if len(Maze[0]) > x >= 0 and 0 <= y < len(Maze[0]):
        if Maze[x][y] == 0:
            return True
    return False


def maze(Maze):
    """
    This function initializes the stack and calls the solve function in order to solve the maze 
    :param Maze: 2 dimensional array
    :return: print path o solution or no solution
    """
    path = Stack()
    past_cells = []

    if solve_maze(Maze, path, past_cells, 0, 0):
        path.display()
    else:
        print("No Solution")


def solve_maze(Maze, path, past_cells, x, y):
    """
    This function gets a maze, a stack, a list and a start point and solves the maze
    according to the instructions given
    :param Maze: 2 dimensional array
    :param path: stack to record the solution path
    :param past_cells: list of cells that the player been through
    :param x: x start point
    :param y: y start point
    :return: true if there is a solution, else false
    """
    if (x, y) == (len(Maze[0]) - 1, len(Maze[0]) - 1):
        path.push((x, y))
        return True

    if is_safe(Maze, x, y):
        if (x, y) not in past_cells:
            path.push((x, y))
            past_cells.append((x, y))
        else:
            return False
    else:
        if (x, y) not in past_cells:
            past_cells.append((x, y))
        return False

    if solve_maze(Maze, path, past_cells, x, y + 1):
        return True
    elif solve_maze(Maze, path, past_cells, x + 1, y):
        return True
    elif solve_maze(Maze, path, past_cells, x, y - 1):
        return True
    elif solve_maze(Maze, path, past_cells, x - 1, y):
        return True
    else:
        last = path.pop()
        add_x = path.peek()[0] - last[0]
        add_y = path.peek()[1] - last[1]
        return solve_maze(Maze, path, past_cells, x + add_x, y + add_y)


if __name__ == '__main__':
    maze1 = [[0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0],
             [0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1],
             [0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0],
             [0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
             [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
             [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
             [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0],
             [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
             [0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
             [0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0],
             [0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
             [1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0],
             [0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
             [0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0]]

    for row in maze1:
        print(row)

    print("The path to walk through is: ")
    maze(maze1)

    maze2 = [[0 for x in range(25)] for y in range(25)]

    for row in maze2:
        print(row)

    print("The path to walk through is: ")
    maze(maze2)
