"""
Authors: Adi Hakimi - 209445121, Nizan Shookroun - 207678699
Date: 03/10/2022
Description: Implementation of binary tree
"""

# this import is only for creating a random binary tree
import numpy


class BinaryTree:
    def __init__(self, data):
        """
        This function initializes a binary tree with root value
        :param data: root value
        """
        self.left = None
        self.right = None
        self.data = data

    def insert(self, data):
        """
        This function inserts a value into a binary tree
        :param data: nw value to insert to binary tree
        """
        if self.data:
            if data < self.data:
                if self.left is None:
                    self.left = BinaryTree(data)
                else:
                    self.left.insert(data)
            elif data > self.data:
                if self.right is None:
                    self.right = BinaryTree(data)
                else:
                    self.right.insert(data)
        else:
            self.data = data

    def is_lbt(self):
        """
        This function checks if a binary tree is Left Binary Tree
        :return: True if LBT, else false
        """
        if self.data is None:
            return True
        else:
            if self.left and self.right:
                return self.left.is_lbt() and self.right.is_lbt()
            elif self.left is None and self.right is not None:
                return False
            elif self.right is None and self.left is not None:
                return self.left.is_lbt()
            else:
                return True

    def get_cross_sum(self):
        """
        This function calculates the number of cross and leaves in a binary tree
        :return: number of crosses and leaves
        """
        if self.data is None:
            return 0
        elif self.left and self.right:
            return self.left.get_cross_sum() + self.right.get_cross_sum() + 1
        elif self.left is not None and self.right is None:
            return self.left.get_cross_sum() + 1
        elif self.left is None and self.right is not None:
            return self.right.get_cross_sum() + 1
        else:
            return 1

    def print_tree(self):
        """
        This function prints the number of crosses nd leaves in every cross of the binary tree
        """
        if self.data is None:
            print([])
        else:
            print(self.get_cross_sum(), end=" ")
            if self.left:
                self.left.print_tree()
            if self.right:
                self.right.print_tree()

    def print_inorder(self):
        """
        This function prints inorder of binary tree
        """
        if self.data:
            if self.left:
                self.left.print_inorder()
            print(self.data, end=" ")
            if self.right:
                self.right.print_inorder()


if __name__ == '__main__':
    tree1 = BinaryTree(5)
    tree1.insert(3)
    tree1.insert(2)
    tree1.insert(1)
    tree1.insert(12)
    tree1.insert(11)
    tree1.insert(14)
    tree1.insert(9)
    tree1.insert(10)
    tree1.insert(8)

    print("inorder print:")
    tree1.print_inorder()
    print()
    if tree1.is_lbt():
        print("Tree 1 i LBT!")
    else:
        print("Tree 1 is not LBT")
    tree1.print_tree()
    print()

    tree2 = BinaryTree(14)
    cross = 20
    cross_numbers = [14]
    print("The order of appending values into the random binary tree is:")
    while cross > 0:
        number = numpy.random.randint(0, 50)
        if number not in cross_numbers:
            cross_numbers.append(number)
            print(number, end=" ")
            tree2.insert(number)
            cross -= 1

    print()
    print("inorder print:")
    tree2.print_inorder()
    print()
    if tree2.is_lbt():
        print("Tree 2 is LBT!")
    else:
        print("Tree 2 is not LBT")
    tree2.print_tree()



